# Install

Install docker (or podman) on your machine.

```
sudo apt install docker.io
```

Add bin to your path in .profile

```
export PATH="$HOME/.var/docker-helper/bin:$HOME/.var/docker-helper/bin/export:$PATH"
```

We don't mind about having root so add user to docker group.
Also add user to the video group for running apps from inside the container.

```
sudo usermod -aG docker $USER
sudo usermod -aG video $USER
```

Then restart the session

# Firewall

Trust the docker interface and restart docker, otherwise when building DNS fails.

```
sudo firewall-cmd --permanent --zone=trusted --change-interface=docker0
sudo firewall-cmd --reload
sudo systemctl restart docker.service
```

# Build

## Common base

First build the common base images you need.

To build a common base specify the name and tag from the folder structure.
Eg to build Debian Stretch Dev base this is in the folder `debian-dev/9`

```
docker-helper-common-build debian-dev 9
```

## Apps

To build an app specify the project name and optionally the tag.

```
docker-helper-build my-project
docker-helper-build tagged-project tag1
```

Note this will then create a my-project executable in the bin/exports folder, which executes the run.sh of the app.

See [APPS.md](APPS.md) for how to structure an app and [SNIPPETS.md](SNIPPETS.md) for common sandbox escapes in your `run.sh` files.

# Run

To run a project, specify the project name and it's optional tag.

```
my-project
tagged-project-tag1
```
