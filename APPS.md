
# Structure

The typical structure of an app could look like the following.

```
/app1
  /tag1
    - build.sh
    - Dockerfile
    - run.sh

/app2
  - build.sh
  - Dockerfile
  - run.sh
```

Apps can optionally have a tag, they then must have a `build.sh` and a `run.sh` which may use a `Dockerfile`.

# Environment

If you have a file called `autoprojectfolders` in your app,
then the following environment variables will be set and folders created.

| Variable | Location |
| -------- | -------- |
| BUILD_FOLDER | `$HOME/.cache/docker-builds/$FOLDER` |
| CODE_FOLDER | `$HOME/Projects/$FOLDER/code` |
| PERSIST_FOLDER | `$HOME/Projects/$FOLDER/persist` |

Where `$FOLDER` is the app name and tag, eg `app1/tag1` or `app2` from the example structures.
