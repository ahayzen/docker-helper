# Example run.sh script

```
CONTAINER=project-container
NAME=project

# Start up systemd
echo "Starting systemd"
docker run \
  --privileged --volume /sys/fs/cgroup:/sys/fs/cgroup:ro
  --detach --name $CONTAINER $NAME

# Not sure if --network="host" is required.

# Login as user
echo "Logging into container $CONTAINER as developer"
docker exec -it $CONTAINER su developer

# Stop systemd and clear up container
echo "Stopping systemd"
docker stop $CONTAINER
docker rm $CONTAINER
```
