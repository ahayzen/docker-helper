
# GDB

```
--cap-add SYS_PTRACE --security-opt seccomp=unconfined
```
  
# Git / SSH

```
--volume "$HOME/.gitconfig:/home/developer/.gitconfig:ro"
--volume "$HOME/.ssh:/home/developer/.ssh:ro"
```
  
# Project

Note that `$BUILD_FOLDER`, `$CODE_FOLDER`, and `$PERSIST_FOLDER` are automatically generated
if you have a file called `autoprojectfolders` in your [app](APPS.md).

```
--volume "$BUILD_FOLDER:/home/developer/build"
--volume "$CODE_FOLDER:/home/developer/src"
--volume "$PERSIST_FOLDER/.bash_history:/home/developer/.bash_history"
--volume "$PERSIST_FOLDER/.config:/home/developer/.config"
```
  
# Pulseaudio

```
--network="host"
--volume /dev/shm:/dev/shm
--volume /etc/machine-id:/etc/machine-id:ro
--volume "$HOME/.pulse:/home/developer/.pulse:ro"
--volume /run/user/1000/pulse:/run/user/1000/pulse:ro
--volume /var/lib/dbus:/var/lib/dbus:ro
```
  
# Systemd

```
--network="host" --privileged
--volume /sys/fs/cgroup:/sys/fs/cgroup:ro
```
  
# X11

```
--device /dev/dri:/dev/dri -e DISPLAY --network="host"

# /dev/dri doesn't get correct permissions on 17.12.1, 18.02 -> < 1.0.0-rc5
# workaround is to add a volume
# https://github.com/moby/moby/issues/36446
--volume /dev/dri:/dev/dri:rw
```
